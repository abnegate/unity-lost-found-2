﻿using UnityEngine;
using System.Collections;

public class BGAudio : MonoBehaviour
{
    public float startVolume = 0f;
    public float maxVolume = 1.0f;
    public float incrementStep = 0.01f;
    public float timeStepSeconds = 0.3f;
    public bool dontDestroyOnLoad;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = startVolume;

        audioSource.Play();

        StartCoroutine(FadeAudioVolume(audioSource, 1));

        if (dontDestroyOnLoad) {
            DontDestroyOnLoad(gameObject);
        }
    }

    private IEnumerator FadeAudioVolume(AudioSource source, int direction)
    {
        Debug.Log($"Volume: {source.volume}");
        for (var i = source.volume; source.volume <= maxVolume && source.volume >= 0f; i += incrementStep * direction) {
            source.volume = i;
            yield return new WaitForSeconds(timeStepSeconds);
        }
    }
}