﻿using UnityEngine;

public class Player : MonoBehaviour, IEntity
{
    public float movementSpeed;
    public float maximumMoveSpeed = 25f;
    public float minimumMoveSpeed = 8f;
    public float attackingRange = 0f;
    public float minimumDamage = 0f;
    public float maximumDamage = 0f;

    public float currentPlayerHealth = 0f;
    public float maximumHealth = 100f;
    public float fov;


    #region IEntity properties
    public EntityType type => EntityType.Player;

    public Vector3 position => gameObject.transform.position;
    public float attackRange => attackingRange;
    public float minDamage => minimumDamage;
    public float maxDamage => maximumDamage;
    public float maxHealth => maximumHealth;
    public float currentHealth
    {
        get => currentPlayerHealth;
        set => currentPlayerHealth = value;
    }
    public bool isDead => currentPlayerHealth <= 0;
    public float moveSpeed
    {
        get => movementSpeed;
        set => movementSpeed = value;
    }
    public float maxMoveSpeed => maximumMoveSpeed;
    public float minMoveSpeed => minimumMoveSpeed;
    public float fieldOfView
    {
        get => fov;
        set => fov = value;
    }
    #endregion

    private void Awake()
    {
        currentPlayerHealth = maximumHealth;
    }

    private void Update()
    {
        if (isDead) {
            Debug.Log("Dead!");
            gameObject.SetActive(false);
            return;
        }
    }

    public void AttackTarget(IEntity target)
    {
        // Can't attack
    }
}
