public enum EntityType
{
    None = 0,
    Player = 1,
    Zombie = 2,
    Any = 500,
}
