﻿public interface IStealthAIEntity : IAIEntity
{
    bool isHiding { get; set; }
}
