﻿using Apex.AI.Components;
using Apex.AI;
using System;
using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Linq;

public class Enemy : MonoBehaviour, IStealthAIEntity, IContextProvider
{
    #region Ranges
    [Header("Ranges")]
    public float playerScanRange = 500f;
    public float hidingSpotScanRange = 500f;
    public float hearingRange = 300f;
    public float stalkingRange = 200f;
    public float chargeRange = 50f;
    public float attackingRange = 5f;
    #endregion

    #region View
    [Header("View")]
    [Range(1, 180)]
    public float fov = 60f;
    #endregion

    #region Speed
    [Header("Speed")]
    [Range(0f, 5f)]
    public float movementSpeed = 1f;
    public float rotateSpeed = 40f;
    public float sprintRotateSpeed = 60f;
    #endregion

    #region Patrol
    [Header("Patrol")]
    public Vector3[] defaultPatrolPoints;
    #endregion

    #region Attack
    [Header("Attack")]
    [Range(0f, 5f)]
    public float attackSphereCastRadius = 0.5f;
    public float minimumDamage = 1f;
    public float maximumDamage = 10f;
    #endregion

    #region Health
    [Header("Health")]
    public float maximumHealth = 100f;
    #endregion

    #region IStealthAIEntity properties
    public bool isHiding { get; set; }
    #endregion

    #region IAIEntity properties
    public Vector3? moveTarget { get; set; }
    #endregion

    #region IEntity properties
    public float fieldOfView
    {
        get => fov;
        set => fov = value;
    }

    public float moveSpeed
    {
        get => movementSpeed;
        set => movementSpeed = value;
    }

    public float currentHealth { get; set; }

    public float maxHealth => maximumHealth;

    public bool isDead => currentHealth <= 0f;

    public Vector3 position => transform.position;

    public EntityType type => EntityType.Zombie;

    public float attackRange => attackingRange;

    public float minDamage => minimumDamage;

    public float maxDamage => maximumDamage;

    public NavMeshAgent navMeshAgent => _navMeshAgent;

    public IEntity attackTarget { get; set; }

    public float scanRange => playerScanRange;

    public bool canCommunicate => true;

    public Vector3[] patrolPoints => defaultPatrolPoints;

    public bool isPatrolling { get; set; }
    public bool isPatrolPaused { get; set; }
    public int currentPatrolIndex { get; set; }
    #endregion IEntity properties

    #region Components
    private EnemyContext _enemyContext;
    private NavMeshAgent _navMeshAgent;
    private Animator _animator;
    #endregion

    #region Animations
    int walkHash = Animator.StringToHash("walkTrigger");
    int runStateHash = Animator.StringToHash("runTrigger");
    int idleStateHash = Animator.StringToHash("idleTrigger");
    int attackHash = Animator.StringToHash("attackTrigger");

    int currentStateHash;
    #endregion

    public IAIContext GetContext(Guid aiId)
    {
        return _enemyContext;
    }

    private void Awake()
    {
        _enemyContext = new EnemyContext(this);
        _navMeshAgent = GetComponentInChildren<NavMeshAgent>();
        _animator = GetComponentInChildren<Animator>();
        currentStateHash = idleStateHash;

        defaultPatrolPoints = GameObject
            .FindGameObjectsWithTag(Tags.patrolPoint)
            .Select(p => p.transform.position)
            .ToArray();

        currentHealth = maxHealth;

        EntityManager.instance.Register(gameObject, this);
    }

    private void Update()
    {
        if (isDead) {
            gameObject.SetActive(false);
            return;
        }
    }


    private void OnDisable()
    {
        EntityManager.instance.Unregister(gameObject);
    }

    public void AttackTarget(IEntity target)
    {
        if (!PhysicsExtensions.ConeCastAll(
            transform.position + transform.forward,
            attackSphereCastRadius,
            transform.forward,
            out var _,
            attackRange,
            60,
            LayersManager.instance.playerLayer)) {
            Debug.Log("Cone cast miss!");
            return;
        }

        if (target == null) {
            return;
        }

        if (currentStateHash != attackHash) {
            _animator.SetTrigger(attackHash);
            currentStateHash = attackHash;
        }

        var damage = UnityEngine.Random.Range(minDamage, maxDamage);
        target.currentHealth -= damage;
        Debug.Log("Dealt damage");
    }

    public void MoveTo(Vector3 destination)
    {
        _navMeshAgent.speed = movementSpeed;

        int animHash;
        switch (movementSpeed) {
            case var x when x >= 0f && x <= 2f:
                animHash = walkHash;
                break;
            case var x when x >= 2f && x <= 5f:
                animHash = runStateHash;
                break;
            default:
                animHash = idleStateHash;
                break;
        }

        if (animHash != currentStateHash) {
            _animator.SetTrigger(animHash);
            currentStateHash = animHash;
        }

        if ((destination - transform.position).sqrMagnitude < 1f) {
            return;
        }

        int mask = _navMeshAgent.areaMask;
        if (NavMesh.SamplePosition(destination, out var hit, 1f, mask)) {
            _navMeshAgent.SetDestination(hit.position);
        }
    }

    public void ReceiveCommunicatedMemory(IList<Observation> observations)
    {
        var count = observations.Count;
        for (int i = 0; i < count; i++) {
            if (ReferenceEquals(observations[i].entity, this)) {
                continue;
            }
            var newObs = new Observation(observations[i], false);
            _enemyContext.memory.AddOrUpdateObservation(newObs, true);
        }
    }
}