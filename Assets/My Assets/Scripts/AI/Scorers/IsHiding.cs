﻿using Apex.AI;

public class IsHiding : ContextualScorerBase<EnemyContext>
{
    public override float Score(EnemyContext context)
    {
        if (((Enemy)context.entity).isHiding) {
            return score;
        }
        return 0f;
    }
}
