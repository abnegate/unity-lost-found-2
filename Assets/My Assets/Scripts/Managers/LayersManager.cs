﻿using UnityEngine;

public sealed class LayersManager : MonoBehaviour
{
    public static LayersManager instance;

    public LayerMask enemiesLayer;
    public LayerMask playerLayer;
    public LayerMask obstacleLayer;
    public LayerMask hidingSpotsLayer;

    private void Awake()
    {
        if (instance == null) {
            instance = this;
            return;
        }

        Debug.LogWarning(ToString() + " multiple " + ToString() + " found in the scene");
        Destroy(gameObject, 0.01f);
    }
}
