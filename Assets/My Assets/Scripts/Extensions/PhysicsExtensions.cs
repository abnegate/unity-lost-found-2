﻿using System.Collections.Generic;
using UnityEngine;

public static class PhysicsExtensions
{
    public static bool ConeCastAll(
        Vector3 origin,
        float maxRadius,
        Vector3 direction,
        out RaycastHit[] hitInfos,
        float maxDistance,
        float coneAngle,
        LayerMask layerMask)
    {
        var sphereCastHits = Physics.SphereCastAll(
            origin - new Vector3(0, 0, maxRadius),
            maxRadius,
            direction,
            maxDistance,
            layerMask);

        var coneCastHitList = new List<RaycastHit>();

        if (sphereCastHits.Length > 0) {
            for (int i = 0; i < sphereCastHits.Length; i++) {
                var hitPoint = sphereCastHits[i].point;
                var directionToHit = hitPoint - origin;
                var angleToHit = Vector3.Angle(direction, directionToHit);

                if (angleToHit < coneAngle) {
                    coneCastHitList.Add(sphereCastHits[i]);
                }
            }
        }

        hitInfos = coneCastHitList.ToArray();

        return hitInfos.Length > 0;
    }
}
