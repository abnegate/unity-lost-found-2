﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TimerComponent : MonoBehaviour
{
    public float countdownTime;
    public bool loop;

    public TextMeshProUGUI displayText;

    public int remainingMinutes => Mathf.FloorToInt(countdownTime / 60);
    public int remainingSeconds => Mathf.FloorToInt(countdownTime % 60);

    public UnityEvent onElapsed;

    private float _startingCountdown;

    private void Awake()
    {
        _startingCountdown = countdownTime;
    }

    private void Update()
    {
        displayText.text = string.Format(
            "{0:00}:{1:00}",
            remainingMinutes,
            remainingSeconds);

        if (countdownTime > 0) {
            countdownTime -= Time.deltaTime;
            return;
        }

        onElapsed.Invoke();

        if (loop) {
            countdownTime = _startingCountdown;
        } else {
            countdownTime = 0;
        }
    }
}
