﻿using UnityEngine;

public class AnimationEventTrigger : MonoBehaviour
{
    public UnityStringEvent onFired;

    public void OnTriggerAnimationEvent(string parameter)
    {
        onFired.Invoke(parameter);
    }
}
